# Curso .NET 🚀

Bem-vindo ao repositório oficial do curso de .NET! Aqui você encontrará todo o material necessário para se tornar um desenvolvedor .NET capacitado e pronto para enfrentar desafios emocionantes.

## Programação das Aulas 📅

### ✅ Aula 1: Introdução e Setup do Ambiente (23/01/24)

- Introdução ao curso, apresentação dos objetivos, metodologia e avaliação. Setup do ambiente de desenvolvimento com Visual Studio, .NET Framework e C#. Revisão de lógica de programação com algoritmos, estruturas de dados, fluxo de controle e funções.

    [Material da aula](./aulas/01/01.md)

### ✅ Aula 2: Lógica de Programação e Algorítimos (30/01/24)

- Revisão de lógica de programação com algoritmos, estruturas de dados, fluxo de controle e funções

    [Material da aula](./aulas/02/02.md)

### ✅ Aula 3: Programação orientada a objetos básica (01/02/24)

- Revisão de programação orientada a objetos básica com C#. Conceitos de Classe, instância, Métodos, Atributos, Construtor, encapsulamento, Herança, polimorfismo e interfaces.

    [Material da aula](./aulas/03/03.md)

### ✅ Aula 4: Programação orientada a objetos II (06/02/24)

- Conceito de classes abstratas, generics, tipos genéricos, métodos genéricos e coleções genéricas. Exemplos e exercícios com generics em C#

    [Material da aula](./aulas/04/04.md)

### ✅ Aula 5: Programação orientada a objetos II (08/02/24)

- Conceitos e princípios SOLID, explicação sobre SRP-Princípio da Responsabilidade Única e conceitos básicos sobre Design Patterns.
Exemplos e exercícios em C#.

    [Material da aula](./aulas/05/05.md)

### Aula 6: Programação orientada a objetos II (15/02/24)

- Conceito de aberto e fechado, princípio de substituição de Liskov, princípio de inversão de dependência e injeção de dependência. Exemplos e exercícios com princípios SOLID e padrões de projeto em C#.

    [Material da aula](./aulas/06/06.md)

### Aula 7: Técnicas de programação I (20/02/24)

- Conceito de arquivos de configuração, appsettings, connectionstrings e seções personalizadas. Exemplos e exercícios com arquivos de configuração.

    [Material da aula](./aulas/07/07.md)

### Aula 8: Técnicas de programação I (22/02/24)

- Conceito de lambdas, expressões lambda, delegates e funções anônimas. Exemplos e exercícios com lambdas em C#.

    [Material da aula](./aulas/08/08.md)