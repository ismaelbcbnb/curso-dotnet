public class Circle : Shape
{
    public double Radius { get; set; }

    public override double Area()
    {
        return Math.Round(Math.PI * Math.Pow(Radius, 2), 2);
    }

    public override string ImprimirForma()
    {
        return ("Forma geom�rica: C�rculo");
    }
}