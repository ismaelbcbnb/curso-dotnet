﻿void PrintArea(Shape shape)
{
    Console.WriteLine($"Area: {shape.Area()}");
}

void ImprimirForma(Shape shape)
{
    Console.WriteLine(shape.ImprimirForma());
}


Rectangle rectangle = new Rectangle { Width = 5, Height = 4 };
Square square = new Square { SideLength = 5 };
Circle circle = new Circle { Radius = 2 };

ImprimirForma(rectangle);
PrintArea(rectangle); 
ImprimirForma(square);
PrintArea(square);   
ImprimirForma(circle);
PrintArea(circle);   


