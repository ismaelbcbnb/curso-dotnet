public class Turma : ITurma
{
    public string Nome { get; set; }
    private List<Aluno> _alunos { get; set; }

    public Turma(string nome)
    {
        Nome = nome;
        _alunos = new List<Aluno>();
    }

    public void AdicionarAluno(Aluno aluno)
    {
        _alunos.Add(aluno);
    }

    public void RemoverAluno(Aluno aluno)
    {
        _alunos.Remove(aluno);
    }

    public List<Aluno> ListarAlunos()
    {
        return _alunos;
    }

    public decimal CalcularMediaGeral()
    {
        decimal soma = 0;
        foreach (var aluno in _alunos)
        {
            soma += aluno.ObterMedia();
        }
        return soma / _alunos.Count;
    }
}