public interface ITurma
{
    void AdicionarAluno(Aluno aluno);
    void RemoverAluno(Aluno aluno);
    decimal CalcularMediaGeral();
}